package com.bios.model;

import com.bios.enums.Estado;
import com.bios.model.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-03-24T14:29:24")
@StaticMetamodel(Ticket.class)
public class Ticket_ { 

    public static volatile SingularAttribute<Ticket, String> texto;
    public static volatile SingularAttribute<Ticket, Estado> estado;
    public static volatile SingularAttribute<Ticket, Usuario> usuario_id;
    public static volatile SingularAttribute<Ticket, Integer> valorizado;
    public static volatile SingularAttribute<Ticket, String> asunto;
    public static volatile SingularAttribute<Ticket, Integer> Id;
    public static volatile SingularAttribute<Ticket, String> nombre;
    public static volatile SingularAttribute<Ticket, String> email;
    public static volatile SingularAttribute<Ticket, Date> actualizado;

}