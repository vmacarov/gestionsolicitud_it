package com.bios.model;

import com.bios.model.Ticket;
import com.bios.model.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-03-24T14:29:24")
@StaticMetamodel(Nota.class)
public class Nota_ { 

    public static volatile SingularAttribute<Nota, Date> fecha;
    public static volatile SingularAttribute<Nota, Usuario> user_id;
    public static volatile SingularAttribute<Nota, Integer> id;
    public static volatile SingularAttribute<Nota, Ticket> ticket_id;
    public static volatile SingularAttribute<Nota, String> cuerpo;

}