/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.enums;

/**
 * Estado en el que se encuentra el ticket.
 * @author pavillion
 */
public enum Estado {
    
    resuelto("R"),
    nuevo("N"),
    esperandoRespuesta("E"),
    enProgreso("P");

    private String estado;

    private Estado(String estado) {
        this.estado = estado;
    }

    public String getPrioridad() {
        return estado;
    }

    public void setPrioridad(String estado) {
        this.estado = estado;
    }
}
