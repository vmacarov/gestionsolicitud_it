/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.enums;

/**
 * La prioridad del ticket.
 * @author pavillion
 */
public enum Prioridad {

    baja("B"),
    media("M"),
    alta("A"),
    critico("C");

    private String prioridad;

    private Prioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

}
