/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.controller;

import com.bios.model.Usuario;

import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 * Controla sesion de Usuarios activas.
 *
 * @author pavillion
 */
@Named //duda si uso uno tengo que usar el otro. van de la mano?
@ViewScoped
public class SeguridadLoginController implements Serializable {

    public void verificarSesion() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            //Tenemos que Castear el objeto usuario.
            //con esto ya lo tenemos almacenado
            Usuario us = (Usuario) context.getExternalContext().
                    getSessionMap().get("usuario");

            //si el user es nulo no se encuentra iniciado sesion.
            if (us == null) {
                context.getExternalContext().redirect("/faces/login/Error.xhtml");
            }
        } catch (Exception e) {
            //implementar log para guardar registro de algun error. Más adelante. 
        }
    }
}
