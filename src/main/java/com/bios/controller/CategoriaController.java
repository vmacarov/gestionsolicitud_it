
package com.bios.controller;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.annotation.PostConstruct;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bios.ejb.CategoriaFacadeLocal;
import com.bios.model.Categoria;
import javax.inject.Inject;



@Named //es valida en jsf a parti de la 2.2 de lo que es inyeccion de dependencias.
@ViewScoped
public class CategoriaController implements Serializable {
    
  @EJB //inyeccion->indicamos que es un ejb para evitarnos hacer un NEW de la interfaz. 
  private CategoriaFacadeLocal categoriaEJB;
  //para poder guardar los objetos o utilizar los datos.
  //@Inject
  private Categoria categoria;

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
  
  //esto se ejecuta luego que el constructor se ejecute. este se define auto. sin que sea especificado.
  @PostConstruct
  public void init(){
      categoria = new Categoria();
  }
  
  //llamo al método create del EJB que se inyectó.
  public void grabarCategoría() {
      try{
          categoriaEJB.create(categoria);
      }catch(Exception e){
          FacesContext.getCurrentInstance().
                  addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                          "ERROR", "no se pudo conectar a la BD"));
      }
  }
    
}
