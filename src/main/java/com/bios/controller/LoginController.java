/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.controller;

import com.bios.ejb.UsuarioFacadeLocal;
import javax.inject.Named;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import com.bios.model.Usuario;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author pavillion
 */
@Named
//@ViewScoped //USO ESTA NOTACION PORQUE VOY HACER UPDATE DE TIPO AJAX Y NO DE TIPO REQUEST.//
@SessionScoped
public class LoginController implements Serializable {

    @EJB //CON ESTA ANOTACION INDICAMOS QUE ES DEL AMBITO DE ESA CAPA.
    private UsuarioFacadeLocal EJBUsuario;

    //defino variable usuario; permite almacenar los valores que ingreso por teclado.
    //@Inject
    private Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
    }

    //SE PUEDE DEFINIR OTRA LOGICA COMO POR EJEMPLO CON JAS; SPRINT SEQUIURITY.
    //definimos String porque necesitamos tener una regla de navegacion, si caso es exitoso.
    //caso contrario quedarnos misma pag e indicar credenciales incorrectas.
    public String iniciarSesion() {
        Usuario user;
        String redireccion = null;
        try {
            //iniciar sesion devuelve un tipo de usuario, entonces return de ese valor.
            user = EJBUsuario.iniciarSesion(usuario);
            //si este existe.
            if (user != null) {
                //Almacenar en la sesion de JSF. Asi controlamos que la sesion se encuentre Vigente o Activa.
                //Usamos el Método Put para almacenar los valores
                FacesContext.getCurrentInstance().getExternalContext().
                        getSessionMap().put("usuario", user);
                //NAVEGACION EXPLICITA! ?faces-redirect=true MUESTRO LA RUTA.
                redireccion = "/paginas/inicio/principal?faces-redirect=true";
            } else {
                FacesContext.getCurrentInstance().
                        addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Usuario Incorrecto"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error Interno"));
        }
        return redireccion;
    }
    
    //destruimos los valores que tenemos en el facesContext
    //en nuestra variable sesion
    public void cerrarSesion () {
        FacesContext.getCurrentInstance().
                getExternalContext().invalidateSession();
    }
    
    public String UserLogeado () {
        Usuario usLogeado = (Usuario) FacesContext.
                getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        return usLogeado.getNombre();
    }
}
