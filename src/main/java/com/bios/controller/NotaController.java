package com.bios.controller;

import com.bios.ejb.NotaFacadeLocal;
import com.bios.model.Nota;
import com.bios.model.Ticket;
import com.bios.model.Usuario;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.faces.view.facelets.FaceletContext;
import javax.inject.Named;

/**
 *
 * @author pavillion
 */
@Named
@ViewScoped
public class NotaController implements Serializable {

    @EJB
    private NotaFacadeLocal EJBNota;

    //@Inject
    private Nota nota;

    //@Inject
    private Usuario usuario;
    
    private Ticket ticket;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Nota getNota() {
        return nota;
    }

    public void setNota(Nota nota) {
        this.nota = nota;
    }
    
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @PostConstruct
    public void init() {
        nota = new Nota();
        usuario = new Usuario();
        ticket = new Ticket();
    }

    public void registrar() {
        try {
            //capturo el usuario que inicio sesion.
            Usuario us = (Usuario) FacesContext.getCurrentInstance().
                    getExternalContext().getSessionMap().get("usuario");
            //paso el usuario
            nota.setUser_id(us);
            //como reconosco el ticket que voy a setear ?
            //ticket.getId();            
            //nota.setTicket_id(ticket);
            EJBNota.create(nota);
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Aviso", "Nota creada con Éxito"));

        } catch (Exception e) {
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            "Error", "Error Inesperado!"));
        }

    }
}
