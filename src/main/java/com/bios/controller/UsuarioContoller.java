/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.controller;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ejb.EJB;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.bios.model.Usuario;
import com.bios.ejb.UsuarioFacadeLocal;


/**
 *
 * @author pavillion
 */
@Named
//Este es al ámbito
@ViewScoped
//@SessionScoped //porque el menu va a ser traido en varias páginas y no es conveniente estar pidiendolo constantemente, si ya se sabe que es el menu correcto y no va a variar. si varia hay que refrescar para actualizar los datos
public class UsuarioContoller implements Serializable {

    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    //@Inject
    private Usuario usuario;
    
    private List<Usuario> usuariosListar;

    public List<Usuario> getUsuariosListar() {
        return usuariosListar;
    }

    public void setUsuariosListar(List<Usuario> usuariosListar) {
        this.usuariosListar = usuariosListar;
    }
    
    //@EJB  // ????necesario
    //private List<SelectItem> listaUser;

    /*public List<SelectItem> getListaUser() {
        this.listaUser = new ArrayList<>();
        //UsuarioFacadeLocal uf = new UsuarioFacade();
        
        //List<Usuario> u = uf.listaUsuario();
        List <Usuario> u = usuarioEJB.listaUsuario();
        
        for (Usuario us : u){
            SelectItem userItem = new SelectItem(usuario.getNombre());
            this.listaUser.add(userItem);
        }
        
        return listaUser;
    }
    */
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /*
     Esta anotacion se va a ejecutar luego que el consttructor halla inicializado
     como el constructor no lo estoy  definiendo java lo toma implicitamente.
     */
    @PostConstruct
    public void init() {
        //instancio mis variables (por buenas practicas)
        //con esto no evitamos nullpounterexception
        usuario = new Usuario();
        //this.listarUsuarios();
        usuariosListar = usuarioEJB.findAll();
    }

    public void registrarUsuario() {
        try {
            usuarioEJB.create(usuario);
            //mensaje de JSF
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Aviso", "Usuario creado con Éxito"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                                    "Error", "Error inesperado!"));
        }
    }

    /*public void listarUsuarios() {
        try {
            listaUser = usuarioEJB.findAll();
        } catch (Exception e) {
            //Mensaje de JSF.S
        }*/
    //}

}
