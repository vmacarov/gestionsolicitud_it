package com.bios.controller;


import java.io.Serializable;
import javax.inject.Named;
import javax.ejb.EJB;
import javax.annotation.PostConstruct;
import com.bios.ejb.TicketFacadeLocal;
import com.bios.ejb.UsuarioFacadeLocal;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import javax.enterprise.context.RequestScoped;

import com.bios.model.Ticket;
import com.bios.model.Usuario;
import com.bios.enums.Estado;
import javax.faces.context.ExternalContext;
import javax.faces.context.Flash;



/**
 *
 * @author pavillion
 */
//ManagedBean(name="TicketController")
//@RequestScoped
@Named
//@ViewScoped
@RequestScoped
public class TicketController implements Serializable {
    
    private Estado estadoTicket;

    public Estado getEstadoTicket() {
        return estadoTicket;
    }

    public void setEstadoTicket(Estado estadoTicket) {
        this.estadoTicket = estadoTicket;
    }
    
    @EJB
    private TicketFacadeLocal EJBTicket;
    @EJB
    private UsuarioFacadeLocal EJBUsuario;  
    //@Inject
    private Ticket ticket;
    
   /* @Inject
    private TicketController envioDatosTicket;

    public TicketController getTicketController() {
        return envioDatosTicket;
    }

    public void setTicketController(TicketController ticketController) {
        this.envioDatosTicket = ticketController;
    }*/
    
    private List<Ticket> ticketListar;

    public List<Ticket> getTicketListar() {
        return ticketListar;
    }

    public void setTicketListar(List<Ticket> ticketListar) {
        this.ticketListar = ticketListar;
    }
    //@Inject
    private Usuario usuario;
    
    private List<Usuario> propietario;

    public List<Usuario> getPropietario() {
        return propietario;
    }

    public void setPropietario(List<Usuario> propietario) {
        this.propietario = propietario;
    }
  
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
 
    @PostConstruct
    public void init(){
        ticket = new Ticket();
        ticket.setEstado(estadoTicket);
        //this.ticket = envioDatosTicket.getTicket();
        usuario = new Usuario();
        //Necesita que sea debuelto en un objeto tipo LIST
        propietario = EJBUsuario.findAll();
        ticketListar = EJBTicket.findAll();
    }

    
    public void agregarTicket () {
        try{
            ticket.setUsuario_id(usuario);
            estadoTicket = Estado.nuevo;
            ticket.setEstado(estadoTicket);
            EJBTicket.create(ticket);    
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Aviso", "Ticket creado con Éxito"));
        }catch(Exception e){
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            "Error", "Error Inesperado!"));            
        }
    }
    
    public void editTicket(Ticket ticket) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try{
            Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
            flash.put("ticket", ticket);
            ec.redirect("/faces/paginas/Ticket/EditTicket.xhtml");
            
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Aviso", "Ticket creado con Éxito"));
        }catch (Exception e) {
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            "Error", "Error Inesperado!"));
        }
    }
    
    public void asignar(Ticket ticket){
        this.ticket = ticket;
    }
/*    public void datosTicket(Ticket ticket) {
        this.ticket = ticket;
    }*/
    
    //tengo que pasarle Usuario propietario ?
    public void editarUsuario(){
        try{
            //propietario.get(List<propietario>);
            ticket.setUsuario_id((Usuario) propietario);
            EJBTicket.edit(ticket);
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Aviso", "Ticket modificado con Éxito"));
        }catch (Exception e){
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            "Error", "Error Inesperado!"));
        }finally {
            //como estamos haciendo redireccion es necesario este bloque.
            //flashskped uno es para mantener mensaje y otro es para enviar mensaje de paguina a otra.
            FacesContext.getCurrentInstance().
                    getExternalContext().getFlash().setKeepMessages(true);
        }
        
        
    }
}
