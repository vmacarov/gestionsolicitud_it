/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.controller;

import com.bios.ejb.TicketFacadeLocal;
import com.bios.model.Ticket;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author pavillion
 */
@Named
@ViewScoped
//@RequestScoped
public class EditController implements Serializable {
    
    @EJB
    private TicketFacadeLocal EJBEditTicket;
      
    @Inject
    private TicketController editarTicket;   
    
    private Ticket ticketEdit;

    public Ticket getTicketEdit() {
        return ticketEdit;
    }

    public void setTicketEdit(Ticket ticketEdit) {
        this.ticketEdit = ticketEdit;
    }

    public TicketController getEditarTicket() {
        return editarTicket;
    }

    public void setEditarTicket(TicketController editarTicket) {
        this.editarTicket = editarTicket;
    }
    
    @PostConstruct
    public void init() {
        //editarTicket = new TicketController();
        this.ticketEdit = editarTicket.getTicket();
        
        //ticketEdit = new Ticket();
        //Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        //this.ticketEdit = (Ticket) flash.get("ticket");       
    }
    
    public void valorizarTicket() {
        try{
            EJBEditTicket.edit(ticketEdit);
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Aviso", "Modificación Exitosa"));
        }catch(Exception e) {
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            "Error", "Error Inesperado!"));
            //importante este bloque para mantener mensajes cuando se esta navegando a otra pagina 
        }finally{
            FacesContext.getCurrentInstance().getExternalContext().
                    getFlash().setKeepMessages(true);
        }
    }
}
