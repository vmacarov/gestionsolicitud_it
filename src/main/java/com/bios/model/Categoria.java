/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.model;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author pavillion
 */
@Entity
@Table(name = "categoria")
public class Categoria implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//identity porque mysql lo hace autoincremental.
    @Column (name = "id", nullable = false)
    private int id;
    
    @Column (name = "nombre")
    private String nombre;
    
    @Column (name = "estado")
    private boolean estado= true;
    //private prioridad; podriamos usar anotacion enumerate

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    //Con esto le voy a dar un nombre o un alias a mi objeto que se va a instanciar de esta clase.
    @Override
    public String toString() {
        return this.getNombre();
    }
    
    
    
}
