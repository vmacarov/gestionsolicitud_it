/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author pavillion
 */
@Entity
@Table (name = "nota")
public class Nota implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    //una nota pueden pertenecer a muchos usuarios
    @ManyToOne 
    @JoinColumn (name = "user_id", referencedColumnName = "id", 
            foreignKey = @ForeignKey(name = "FK_nota_usuario"))
    private Usuario user_id;
    
    @ManyToOne 
    @JoinColumn (name = "ticket_id", referencedColumnName = "id", 
            foreignKey = @ForeignKey(name = "FK_nota_ticket"))
    private Ticket ticket_id;    
    
    @Column(name = "cuerpo")
    private String cuerpo;
    
   // @Column(name = "valorizacion")
   // private short valorizacion;
    
    //insertable true, es considerado en el insert de jpql
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", insertable = false)
    private Date fecha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public Ticket getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(Ticket ticket_id) {
        this.ticket_id = ticket_id;
    }

    public Usuario getUser_id() {
        return user_id;
    }

    public void setUser_id(Usuario user_id) {
        this.user_id = user_id;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    /*public short getValorizacion() {
        return valorizacion;
    }

    public void setValorizacion(short valorizacion) {
        this.valorizacion = valorizacion;
    }*/

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    

    @Override
    public String toString() {
        return "com.bios.model.Nota[ id=" + id + " ]";
    }
    
}
