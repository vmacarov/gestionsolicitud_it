package com.bios.model;

import com.bios.enums.Estado;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
//import org.eclipse.persistence.annotations.Index;

/**
 * @author pavillion
 */
@Entity
@Table(name = "ticket")//, 
//        indexes = {@javax.persistence.Index(name = "ticket_nombre", columnList = "nombre")})
public class Ticket implements Serializable {

    //seguimiento del ticket + cale. 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int Id;

    //muchos ticket pertenecen a un usuario.
    //el ticket tiene solo un asignado. puede ser editado.
    @ManyToOne
    @JoinColumn(name = "usuario_id", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "ticket_ibfk_1"))
    //@Column (name = "usuario_id", nullable = false)
    private Usuario usuario_id;

    //cuando entra una solicitud dentro de que categoria va?
    //private Cliente cliente;
    @Column(name = "nombre", nullable = false)
    @Size(max = 50)
    private String nombre;//descripcion

    @Column(name = "email", nullable = false)
    @Size(max = 50)
    private String email; //correo fk a usuario?    

    @Column(name = "asunto")
    @Size(max = 50)
    private String asunto;

    @Column(name = "texto")
    @Size(max = 1024)
    private String texto;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "estado")
    private Estado estado;

    //desarrollo o soporte...
    //private Categoria categoria;
    
    //enum?? USAR ANOTACION ENUMERATE ??
    //private Prioridad prioridad;// 5 tipos.
    
    //@DateTimeFormat ( pattern = "yyyy-MM-dd" )
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "actualizado", insertable = false)
    private Date actualizado; //ultima modificacion.

    /*@Temporal(TemporalType.DATE)
     @Column (name = "nombre")
     private Date fechaCierre;
     */
    //private String ultimaRespuesta; // setea usuario.
    
    @Column (name = "valorizado")
    private Integer valorizado; // a pedido de bios.

    public Integer getValorizacion() {
        return valorizado;
    }

    public void setValorizacion(Integer valorizacion) {
        this.valorizado = valorizacion;
    }
    
   // private String nota; //hay que ver si creo tabla para esto.
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Usuario getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Usuario usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }

    /* public Date getFechaCierre() {
     return fechaCierre;
     }

     public void setFechaCierre(Date fechaCierre) {
     this.fechaCierre = fechaCierre;
     }
     */
    @Override
    public String toString() {
        return this.getNombre();
       //String sNombreClase = this.getClass().getName();
        //int iUltimoPunto = sNombreClase.lastIndexOf(".");
       //sNombreClase = sNombreClase.substring(iUltimoPunto+1);
      //return  this.getId() + "" + this.getUsuario_id() + "" 
        //      + ""+ this.getNombre() + "" + this.getAsunto() +""+ this.getActualizado();
    }

}
