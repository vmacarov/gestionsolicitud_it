/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.ejb;

import com.bios.model.Ticket;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author pavillion
 */
@Stateless
public class TicketFacade extends AbstractFacade<Ticket> implements TicketFacadeLocal {

    @PersistenceContext(unitName = "gsitPool")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TicketFacade() {
        super(Ticket.class);
    }
   /* 
    @Override
    public Ticket agregarTicket(Ticket ticket) {
        Ticket nuevoTicket = null;
        String Insert;
        try{
            
        }catch(Exception e) {
            throw e;
        }
        return ticket;
    }
*/
}
