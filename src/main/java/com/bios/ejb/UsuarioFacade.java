/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.ejb;

import com.bios.model.Usuario;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author pavillion
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "gsitPool")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public Usuario iniciarSesion(Usuario user) {
        Usuario usuario = null;
        String consulta;
        try {
            //SELECT u: no es necesario usamos toda la Data.
            consulta = "FROM Usuario u WHERE u.nombre = ?1 and u.password = ?2";
            Query query = em.createQuery(consulta);
            query.setParameter(1, user.getNombre());
            query.setParameter(2, user.getPassword());
            //obtenemos lista de esta consulta.
            List<Usuario> lista = query.getResultList();
            //obtenemos el objeto unico.
            //query.getSingleResult();
            //si la lista es diferente de vacía hay un elemento:
            if (!lista.isEmpty()) {
                //primer elemento de la lista.
                usuario = lista.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        //no es necesario hacer esto porque usamos EJB si lo hacemos tenemos exception.
        //Si usaramos JPA (emptityManagerFactory) debemos cerrar sea framwork que sea. 
        /*finally{
         em.close();
         }*/
        return usuario;
    }

    @Override
    public List<Usuario> listaUsuario() {
       List<Usuario> listarUsuarios = null;
       String consulta;
       try {
           consulta = "FROM Usuario";
           Query query = em.createQuery(consulta);
           query.getParameter(consulta);
           //obtenemos la lista
     
           List<Usuario> lista = query.getResultList();
           
       }catch(Exception e) {
           throw  e;
       }
       //retornamos una lista de usuarios.
       return listarUsuarios;
    }

}
