/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bios.ejb;

import com.bios.model.Usuario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author pavillion
 */
@Local
public interface UsuarioFacadeLocal {
    
    Usuario iniciarSesion(Usuario user);
    
    public List<Usuario> listaUsuario();

    void create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);

    Usuario find(Object id);

    List<Usuario> findAll();

    List<Usuario> findRange(int[] range);

    int count();
    
}
