package com.bios.ejb;

import com.bios.model.Categoria;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author pavillion
 */
@Stateless
public class CategoriaFacade extends AbstractFacade<Categoria> implements CategoriaFacadeLocal {

    @PersistenceContext(unitName = "gsitPool")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    //Constructor tiene método super (hereda) de abstractfacade que le pasa la categoria. y va a buscar su contructor padre.
    public CategoriaFacade() {
        super(Categoria.class);
    }

}
